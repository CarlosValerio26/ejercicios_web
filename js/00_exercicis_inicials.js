//ASIGNACIONES Y CALCULOS
// EJERCICIO 1: DECIR CUAL ES EL PRIMER Y SEGUNDO NUMERO 
//function numeros(x,y){
// console.log("El primer numero es " +x+ " y el segundo numero es " +y)
// }
// console.log(numeros(3,5));

//EJERCICIO 2: BUSCAR EL NUMERO MAS GRANDE
// function mayor(a,b,c){
//     if(a>b && a>c){
//         console.log("El numero mayor es " +a);
//     }else if(b>a && b>c){
//         console.log("El numero mayor es " +b);
//     }else{
//         console.log("El numero mayor es " +c);
//     }
// }
// console.log(mayor(1,2,3));

//EJERCICIO 3: CALCULAR SUMA,RESTA,MULTIPLICACION Y DIVISION 
// function calcula(a,b){
//     console.log("a+b=" +(a+b));
//     console.log("a-b=" +(a-b));
//     console.log("a*b=" +(a*b));
//     console.log("a/b=" +(a/b));
// }
// console.log(calcula(2,2));

//EJERCICIO 4: MOSTRAR EL PROCENTAJE DE CHICAS EN CLASE
// function xx(chicos,chicas){
//     var porcentaje= ((chicas*100)/chicos);
//     console.log("El porcentaje de chicas en clase es " +porcentaje)
// }
// console.log(xx(20,0));


//COMPARACIONES
//EJERCICIO 1: COMPARAR DOS NUMEROS Y DETERMINAR CUAL ES EL MAYOR Y INDICAR SI SON IGUALES
// function compara(a,b){
//     if(a>b){
//         console.log("El numero mayor es " +a);
//     }else if(b>a){
//         console.log("El numero mayor es " +b);
//     }else{
//         console.log("Son iguales");
//     }
// }
// console.log(compara(1,1));


// EJERCICIO 2: PEDIR DOS NUMEROS Y UNA OPERACION A EFECTUAR, YA SEA "M" DE MULTIPLICAR O "S" DE SUMAR,MOSTRAR LA OPERACION ESCOGIDA Y EL RESULTADO
// function operacion(a, b, c) {
//     if (c == "m") {
//         console.log("a*b=" + (a * b));
//     }
//     if (c == "s") {
//         console.log("a+b=" + (a + b));
//     }
// }
// console.log(operacion(1, 2, "s"));

//EJERCICIO 3: INDICAR SI EL AÑO ES BISIESTO
//  -QUE SEA DIVISIBLE POR 4 Y NO DIVISIBLE POR 100
//  -QUE SEA DIVISIBLE POR 400
// function anyo(num) {
//     if (num % 4 == 0 && num % 4 != 100 || num % 400 == 0) {
//         console.log("El año es bisiesto");
//     } else {
//         console.log("El año no es bisiesto");
//     }
// }
// console.log(anyo(2017));

// ARRAYS/ LISTAS/ BUCLES
// EJERCICIO 1: RECIBIR UN ARRAY Y CALCULAR LOS SIGUIENTES RESULTADOS:
// function calculos(arr) {
//     var total = 0, promedio, valoresPares = 0,
//         valoresImpares = 0, cont = 0;

//     //ASUMIMOS QUE EL PRIMER NUMERO ES EL MAYOR
//     var valorMasAlto = arr[0];
//     var valorMasBajo = arr[0];

//     for (var i = 0; i < arr.length; i++) {
//         //BUSCAMOS LOS VALORES MAS GRANDES Y EL MAS PEQUEÑO
//         if (arr[i] > valorMasAlto) {
//             valorMasAlto = arr[i];
//         }
//         if (arr[i] < valorMasBajo) {
//             valorMasBajo = arr[i];
//         }

//         //BUSCAMOS LOS NUMEROS PARES E IMPARES
//         if (arr[i] % 2 == 0) {
//             valoresPares++;
//         } else {
//             valoresImpares++;
//         }

//         //CALCULAMOS EL TOTAL
//         total = total + arr[i];
//         cont = cont + 1;
//     }

//     promedio = total / cont;
//     console.log("La suma total es " + total);
//     console.log("El promedio es " + promedio);
//     console.log("El valor mas alto es " + valorMasAlto);
//     console.log("El valor mas bajo es " + valorMasBajo);
//     console.log(valoresPares + " Valor/es par/es");
//     console.log(valoresImpares + " Valor/es  impar/es ");
// }
// console.log(calculos([1, 2, 3, 44, 55, 6]));

//EJERCICIO 2: RECIBIR UN STRING Y INVERTIRLO
// function gira(b) {
//     //VARIABLE QUE ALMACENARA LAS LETRAS DE LA CADENA A LA INVERSA
//     var cadenaInvertida = "";
//     //CON b.length ALMACENAMOS EL ULTIMO VALOR DE LA CADENA, CUANDO EMPEXAMOS A RECORRER EL ARRAY, LO HACE A LA INVERSA GRACIAS AL i--
//     for (var i = b.length; i >= 0; i--) {
//         //CON EL charAt CONSEGUIMOS OBTENER LAS LETRAS DEL ARRAY
//         cadenaInvertida += b.charAt(i);
//     }
//     console.log(cadenaInvertida);
// }
// console.log(gira("barcelona"));

// EJERCICIO 3: RECIBE DOS ARRAY x Y y Y DEVUELVE LA DISTANCIA ENTRE LOS DOS PUNTOS
// function distancia(x, y) {
//     var hipotenusa = 0, hipotenusaX = 0, hipotenusaY = 0;

//     for (var i = 0; i < x.length; i++) {
//         hipotenusaX = Math.sqrt((x[i] * x[i]));
//     }
//     for (var j = 0; j < y.length; j++) {
//         hipotenusaY = Math.sqrt((y[j] * y[j]));

//     }
//     var hipotenusa = hipotenusaX + hipotenusaY;
//     console.log("La distancia entre dos puntos es " + hipotenusa);
// }
// console.log(distancia([1, 1], [2, 2]));


//JSON/LISTAS
//EJERCICIO 1: RECIBE LISTA( ARRAY) DE MOTOS Y UN STRING INDICANDO LA MARCA QUE VAMOS A MOSTRAR, Y CUANTAS HAY
// var myObj = [
//     {
//         "model": "YAMAHA X-MAX 250",
//         "preu": 1300,
//         "kilometres": 68000,
//         "cilindrada": 249,
//         "lloc": "Barcelona",
//         "any": 2007
//     },
//     {
//         "model": "SUZUKI BURGMAN 200",
//         "preu": 1200,
//         "kilometres": 45000,
//         "cilindrada": 200,
//         "lloc": "Barcelona",
//         "any": 2007
//     },
//     {
//         "model": "HONDA FORZA 250 X",
//         "preu": 1599,
//         "kilometres": 15000,
//         "cilindrada": 250,
//         "lloc": "Barcelona",
//         "any": 2007
//     },
//     {
//         "model": "HONDA FORZA 250 X",
//         "preu": 1599,
//         "kilometres": 15000,
//         "cilindrada": 250,
//         "lloc": "Barcelona",
//         "any": 2007
//     },
//     {
//         "model": "SUZUKI BURGMAN 200",
//         "preu": 1200,
//         "kilometres": 45000,
//         "cilindrada": 200,
//         "lloc": "Barcelona",
//         "any": 2007
//     },
//     {
//         "model": "YAMAHA X-MAX 250",
//         "preu": 1300,
//         "kilometres": 68000,
//         "cilindrada": 249,
//         "lloc": "Barcelona",
//         "any": 2007
//     },
//     {
//         "model": "YAMAHA X-MAX 250",
//         "preu": 1300,
//         "kilometres": 68000,
//         "cilindrada": 249,
//         "lloc": "Barcelona",
//         "any": 2007
//     }

// ];
// var i, cont = 0;

// for (i in myObj) {
//     var myJSON = JSON.stringify(myObj[i]);
//     localStorage.setItem("testJSON", myJSON);


//     var text = localStorage.getItem("testJSON");
//     var obj = JSON.parse(text);
//     if (obj.model.includes("HONDA")) {
//         cont++;
//         console.log(obj.model);
//     }
// }
// console.log("Hay " + cont + " modelos de la marca HONDA");


//EJERCICIO 2: RECIBE UNA LISTA(ARRAY) DE MOTOS Y DOS VALORES NUMERICOS INDICANDO PRECIO MINIMO Y MAXIMO
// function listaPrecio(lista, y, z) {
//     var i, cont = 0;
//     for (i in lista) {
//         var myJSON = JSON.stringify(lista[i]);
//         localStorage.setItem("testJSON", myJSON);

//         var text = localStorage.getItem("testJSON");
//         var obj = JSON.parse(text);
//         if (obj.preu >= y && obj.preu <= z) {
//             cont++;
//             console.log(obj.model);
//         }
//     }
//     console.log("Hay " + cont + " modelos entre los precios " + y + " y " + z);
// }
// console.log(listaPrecio(myObj, 1000, 2000));

// EJERCICIO 2: RECIBE UNA LISTA (ARRAY) DE MOTOS Y DEVUELVE LOS SIGUIENTES DATOS:
// - moto más cara (modelo y precio)
// - moto más económica (modelo y precio)
// - moto con mas km (modelo, precio y km)
// - marca más repetida (marca, núm de motos)

// function analiza(lista) {
//     var j, i;
//     var precioMasAlto = lista[0].preu;
//     var precioMasEconomico = lista[0].preu;
//     var motoMasKm = lista[1].kilometres;
//     var modelRepetido = lista[0].model;
//     var contLetraRepetida = 0;

//     for (i in lista) {
//         //SABER QUE HACE AQUI!!!!
//         var myJSON = JSON.stringify(lista[i]);
//         localStorage.setItem("testJSON", myJSON);
//         //SABER QUE HACE AQUI!!!!
//         var text = localStorage.getItem("testJSON");
//         var obj = JSON.parse(text);

//         if (obj.preu > precioMasAlto) {
//             console.log(obj.model + " __ " + obj.preu + "€");
//         }
//         if (obj.preu < precioMasEconomico) {
//             console.log(obj.model + " __ " + obj.preu + "€");
//         }
//         if (obj.kilometres > motoMasKm) {
//             console.log(obj.model + " __ " + obj.preu + "€ " + obj.kilometres + "km");
//         }
//         var cont = 0;
//         for (j in lista) {
//             //  SABER QUE HACE AQUI!!!!
//             var myJSON2 = JSON.stringify(lista[j]);
//             localStorage.setItem("testJSON2", myJSON2);
//             //  SABER QUE HACE AQUI!!!!
//             var text2 = localStorage.getItem("testJSON2");
//             var obj2 = JSON.parse(text2);

//             //COMPARAMOS LOS DOS OBJETOS, SON EL MISMO OBJETO POR CIERTO!!!!
//             // SI SON IGUALES PUES EL CONTADOR AUMENTA
//             if (obj.model == obj2.model) {
//                 cont++;
//             }
//         }
//         //SI EL contLetraRepetida ES MAS PEQUEÑO QUE EL cont
//         if (contLetraRepetida < cont) {
//             //PUES ALMACENAMOS EL VALOR DE cont EN EL contLetraRepetida, EL CUAL ALMACENARA LA LETRA QUE MAS SE REPITE  
//             contLetraRepetida = cont;
//             //LE PASAMOS EL SEGUNDO OBJETO AL modelRepetido, QUE ES QUIEN CONTIENE LA MARCA INICIAL LA CUAL SERA COMPARADA
//             modelRepetido = obj2.model;
//         }
//     }
//     console.log(modelRepetido.substring(0, modelRepetido.indexOf(" ")) + " se repite " + contLetraRepetida);
// }
// console.log(analiza(myObj));

// ESTO ES UN BUEN EJEMPLO!!!!!
//CONSEGUIREMOS QUE SE ELIMINEN TODOS LOS VALORES REPETIDOS
// let arrayx = ["a", "b", "c","c"];
// let diferents = new Set();
// arrayx.forEach(el=>diferents.add(el));
// let cosa= Array.from(diferents);
// console.log(cosa);

//BUCLES
//EJERCICIO 1: PEDIR UN NUMERO ENTRE 1 Y 10, SI NO ES UN NUMERO DE ESTE RANGO MOSTRAR UN ERROR Y PEDIRLO DE NUEVO. SI ES CORRRECTO MOSTRAR EL CUADRADO DEL VALOR INTRODUCIDO
// function a(num) {

//     while (num < 1 || num > 10) {
//         num = prompt("No estas en el rango solicitado. Introduce el numero de nuevo:")
//         console.log(num);
//         if (num >= 1 && num <= 10) {
//             console.log("El numero " + num + " es correcto")
//         }
//     }
// }
// var numero = prompt("Ingresa un numero entre 1 y 10");
// console.log(a(numero));

// EJERCICIO 2: PEDIR NUMEROS HASTA QUE EL VALOR INTRODUCIDO SEA <=0. AL FINAL DEL PROCESO, MOSTRAR LA SUMA DE LOS VALORES INTRODUCIDOS
// function a(num) {
//     var sumaNumeros = 1;
//     while (num != 0) {
//UTILIZAMOS Number PARA REFERIRNOS A LA INTRODUCCION DE NUMEROS 
//         num = Number(prompt("Introduce un nuevo numero, si quieres sumarlos introduce 0"));
//         sumaNumeros = sumaNumeros + num;
//     }
//     if (num == 0) {
//         console.log("La suma de los valores introducidos es " + sumaNumeros);
//     }
// }
// var numero = prompt("Introduce un numero");
// console.log(a(numero));

//EJERCICIO 3: PEDIR NUMERO HASTA QUE EL VALOR INTRODUCIDO SEA <=0, AL FINAL DEL PROCESO, MOSTRAR EL MAYOR, EL MENOR Y LA MEDIA ARITMETICA DE LOS VALORES INTRODUCIDOS. SI NO SE HA INTRODUCIDO AL MENOS UN NUMERO, MOSTRAR UN ERROR(SI EL PRIMERO FUESE CERO)
// function a(num) {
//     var numMayor = 1, numMenor = 1, cont = 1, media, sumaNumeros = 1;
//     while (num > 0) {
//         num = Number(prompt("Introduce un numero mayor a 0"));
//         if (numMayor < num) {
//             numMayor = num;
//         }
//         if (numMenor > num) {
//             numMenor = num;
//         }
//         sumaNumeros += num;
//         cont++;

//     }
//     media = sumaNumeros / cont;
//     console.log("El numero " + numMayor + " es el mas grande");
//     console.log("El numero " + numMenor + " es el mas pequeño");
//     console.log("La media es " + media);

// }
// var numero = prompt("Introduce un numero");
// console.log(a(numero));

//EJERCICIO 4: CALCULAR EL PRODUCTO FACTORIAL DE EL NUMERO INTRODUCIDO
// function a(num) {
//     var calculoFactorial = 1;
//     for (var i = num; i > 0; i--) {
//         calculoFactorial *= i;
//     }
//     console.log("El producto factorial de " + num + " es " + calculoFactorial);
// }
// var numero = Number(prompt("Introduce un numero, para calcular su factorial"));
// console.log(a(numero));

//EJERCICIO 5: SUMA DE LOS n PRIMEROS NUMEROS ENTEROS PARES, EMPEZANDO POR 0
// function n(num) {
//     var sumaPares = 0;
//     for (var i = 0; i < num; i++) {
//         if (i % 2 == 0) {
//             sumaPares += i;
//         }
//     }
//     console.log("La suma de los numeros pares es " + sumaPares);
// }
// var numero = Number(prompt("Introduce un numero"));
// console.log(n(numero));


//EJERCICIO 6: PEDIR UNA CONTRASEÑA 3 VECES, SI NO SE HA ACERTADO, DENEGAR ACCESO. SI SE HA ACERTADO DECIR CONTRASEÑA CORRECTA Y EL NOMBRE DE LA CONTRASEÑA
// function p(cadena) {
//     var intentos = 3;
//     var contrasenia = "rebeca";
//     while (intentos != 0) {
//         intentos--;
//         if (cadena != contrasenia) {
//             cadena = prompt("Introduce la contraseña nuevamente, tienes " + intentos + " intentos de 3");
//         }else{
//             console.log("Contraseña correcta");
//         }
//     }
// }
// var cadena = prompt("Introduce una contraseña");
// console.log(p(cadena));

//EJERCICIO 7: PEDIR UN NUMERO DE SEGUNDOS Y ESCRIBIR UNA CUENTA ATRAS HASTA CERO, ESPERANDO EL TIEMPO ADECUADO ENTRE CADA NUMERO ESCRITO. PODEIS UTILIZAR LAS ORDENES:
//  - ESPERAR 1 SEGUNDO
//  - ESCRIBIR X SIN SALTAR O LIMPIAR PANTALLA
// setTimeout(function (num) {

//     while (num != 0) {
//         console.log(num );
//         num--;
//     }
// }, 1000);